from django.apps import AppConfig


class NotifyConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'notify'
    verbose_name = "Рассылка"
    verbose_name_plural = "Рассылки"

    def ready(self):
        from . import signals
