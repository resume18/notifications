from django.conf import settings
from django.contrib import admin, messages
from django.contrib.admin import AdminSite
from django.contrib.auth import login
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.http import HttpRequest
from django.shortcuts import redirect
from django.urls import path

from notify import models
from notify.services import get_access_token, get_user_info


class DistributionAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "start_time",
        "end_time",
        "text",
        "tags",
        "mobile_operator_codes",
        "created_at",
        "updated_at",
        "statistic_sent",
        "statistic_not_sent"
    )

    @admin.display(description="Тэги")
    def tags(self, obj: models.Distribution):
        return obj.selection_data.get("tag__in")

    @admin.display(description="Коды мобильных операторов")
    def mobile_operator_codes(self, obj):
        return obj.selection_data.get("mobile_operator_code__in")

    @admin.display(description="Отправлено")
    def statistic_sent(self, obj: models.Distribution):
        return obj.message_set.filter(status=models.Message.Status.SENT).count()

    @admin.display(description="Не отправлено")
    def statistic_not_sent(self, obj):
        return obj.message_set.filter(status=models.Message.Status.NOT_SENT).count()


class MyAdminSite(AdminSite):
    login_template = "my_admin/admin.html"
    final_catch_all_view = False

    def request_token(self, request: HttpRequest, *args, **kwargs):
        if request.GET.get("error") or not request.GET.get("code"):
            messages.error("Some error occurred please try again")
            return redirect("admin:login")
        code = request.GET.get("code")
        redirect_to = request.build_absolute_uri(request.path)
        try:
            token = get_access_token(code, redirect_uri=redirect_to)
            info = get_user_info(token)
        except ValidationError as e:
            messages.error(e.message)
            return redirect("admin:login")

        user = User.objects.filter(email=info["email"]).first()
        error = ""
        if not user:
            error = "User with such email not found."
        elif not user.is_active:
            error = "This account inactive.",
        elif not user.is_staff:
            error = "Permission denied."

        if error:
            messages.error(request, error)
            return redirect('admin:login')

        login(request, user)
        return redirect("admin:index")

    def login(self, request, extra_context=None):
        extra_context = {"GOOGLE_CLIENT_ID": settings.GOOGLE_CLIENT_ID}
        return super().login(request, extra_context)

    def get_urls(self):
        urls = super().get_urls()
        urls.append(path("login/request_token/", self.request_token, name="request-token"))
        return urls


admin_site = MyAdminSite()


admin_site.register(models.Distribution, DistributionAdmin)
admin_site.register(models.Client)
admin_site.register(models.Message)
