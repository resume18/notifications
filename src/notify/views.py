from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.mixins import UpdateModelMixin, CreateModelMixin, DestroyModelMixin
from .logging import GenericViewSetWithLogging as GenericViewSet
from .serializers import ClientSerializer, DistributionSerializer, StatisticSerializer
from .services import get_all_clients, get_all_distributions, get_common_statistic, get_detail_statistic


class ClientViewSet(UpdateModelMixin, DestroyModelMixin, CreateModelMixin, GenericViewSet):
    serializer_class = ClientSerializer
    queryset = get_all_clients()


class DistributionViewSet(UpdateModelMixin, DestroyModelMixin, CreateModelMixin, GenericViewSet):
    serializer_class = DistributionSerializer
    queryset = get_all_distributions()

    @swagger_auto_schema("GET", responses={200: StatisticSerializer(many=True)})
    @action(["GET"], detail=False)
    def common_statistic(self, request):
        statistic = get_common_statistic(distributions=self.queryset)
        response = StatisticSerializer(statistic, many=True).data
        return Response(response)

    @swagger_auto_schema("GET", responses={200: StatisticSerializer()})
    @action(["GET"], detail=True)
    def detail_statistic(self, request, pk):
        statistic = get_detail_statistic(self.get_object())
        response = StatisticSerializer(statistic).data
        return Response(response)
