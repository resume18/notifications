from json import JSONDecodeError

import requests
from django.core.exceptions import ValidationError
from django.db.models import Count
from django.conf import settings

from .models import Client, Distribution, Message


def get_all_clients():
    return Client.objects.all()


def get_all_distributions():
    return Distribution.objects.all()


def get_clients_by_selection_data(**selection_data):
    return Client.objects.filter(**selection_data)


def get_distribution_by_id(distribution_id):
    return Distribution.objects.get(id=distribution_id)


def create_message(distribution, client):
    return Message.objects.create(distribution=distribution, client=client)


def update_message_status(message_id):
    m = Message.objects.filter(id=message_id)
    m.status = Message.Status.SENT
    m.save()
    return m


def get_detail_statistic(distribution):
    return {
        "distribution": distribution,
        "statistic": distribution.message_set.values('status').annotate(count=Count("id"))
    }


def get_common_statistic(distributions):
    result = []
    for distribution in distributions:
        result.append(get_detail_statistic(distribution))
    return result


def get_access_token(code, redirect_uri):
    data = {
        "code": code,
        "client_id": settings.GOOGLE_CLIENT_ID,
        "client_secret": settings.GOOGLE_CLIENT_SECRET,
        "redirect_uri": redirect_uri,
        "grant_type": "authorization_code",
    }
    response = requests.post(settings.GOOGLE_ACCESS_TOKEN_OBTAIN_URL, data=data)
    if not response.ok:
        raise ValidationError("Failed to obtain access token.")
    try:
        response = response.json()
    except JSONDecodeError:
        raise RuntimeError("Failed to retrieve user information")
    try:
        access_token = response["access_token"]
    except KeyError:
        raise RuntimeError("No access token in the response")

    return access_token


def get_user_info(access_token: str):
    response = requests.post(
        settings.GOOGLE_USER_INFO_URL, data={"access_token": access_token}
    )
    if not response.ok:
        raise ValidationError("Failed to obtain user information.")
    try:
        user_info = response.json()
    except JSONDecodeError:
        raise ValidationError("Failed to retrieve user information")

    return user_info
