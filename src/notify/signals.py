from datetime import datetime

from django.db.models.signals import post_save
from django.dispatch import receiver

from notify import services
from .tasks import send_message
from notify.models import Distribution

import logging


logger = logging.getLogger(__name__)


@receiver(post_save, sender=Distribution, dispatch_uid="start_distribution")
def start_distribution(instance: Distribution, created, **kwargs):
    if created:
        clients = services.get_clients_by_selection_data(**instance.selection_data)
        for c in clients:
            message = services.create_message(instance, c)
            logger.info(f"Message {message.id} created.")
            # seconds for expires, because celery bug
            send_message.apply_async(
                (instance.id, message.id, c.phone_number.as_e164),
                eta=instance.start_time,
                expires=(instance.end_time - datetime.now(tz=instance.end_time.tzinfo)).total_seconds()
            )
