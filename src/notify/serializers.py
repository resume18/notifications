from django.core.exceptions import ValidationError
from rest_framework import serializers
from rest_framework.exceptions import ValidationError as DRFValidationError

from .models import Client, Distribution, Message


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'
        read_only_fields = ("mobile_operator_code", )


class SelectionDataSerializer(serializers.Serializer):
    tag__in = serializers.ListField(required=False)
    mobile_operator_code__in = serializers.ListField(required=False, child=serializers.IntegerField())

    class Meta:
        fields = "__all__"


class DistributionSerializer(serializers.ModelSerializer):
    selection_data = SelectionDataSerializer(required=False)

    def validate(self, data):
        if data["start_time"] > data["end_time"]:
            raise DRFValidationError(detail="End time must be greater than start time.")
        return data

    def save(self, **kwargs):
        try:
            instance = super().save(**kwargs)
        except ValidationError as e:
            raise DRFValidationError(detail=e.message)
        else:
            return instance

    class Meta:
        model = Distribution
        fields = '__all__'


class MessageStatisticSerializer(serializers.Serializer):
    status = serializers.ChoiceField(choices=Message.Status.choices)
    count = serializers.IntegerField()


class StatisticSerializer(serializers.Serializer):
    distribution = DistributionSerializer()
    statistic = MessageStatisticSerializer(many=True)
