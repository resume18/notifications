from .views import ClientViewSet, DistributionViewSet
from rest_framework.routers import SimpleRouter


router = SimpleRouter()

router.register("client", ClientViewSet)
router.register("distribution", DistributionViewSet)


urlpatterns = [
    *router.urls
]
