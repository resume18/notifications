import logging

from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

logger = logging.getLogger(__name__)


class GenericViewSetWithLogging(GenericViewSet):
    def finalize_response(self, request: Request, response: Response, *args, **kwargs):
        for_log = {
            "serializer": self.serializer_class,
            "action": self.action,
            "method": request.method,
            "response_code": response.status_code,
            "view": f"{self}.{self.get_view_name()}"
        }

        if self.action == "create":
            if isinstance(response.data, dict):
                for_log["id"] = response.data.get("id")
        if self.action == "list":
            if isinstance(response.data, list):
                for_log["id"] = [item.get("id") for item in response.data]
        else:
            for_log["params"] = args, kwargs

        if hasattr(self.serializer_class, "Meta"):
            if hasattr(self.serializer_class.Meta, "model"):
                for_log["model"] = self.serializer_class.Meta.model
        logger.info(for_log)
        return super().finalize_response(request, response, *args, **kwargs)
