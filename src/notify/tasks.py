import json

from django.contrib.auth.models import User
from django.core.mail import send_mail
from requests import RequestException
from notifications.celery import celery_app
from . import services
from django.conf import settings
import requests
import logging

from .serializers import StatisticSerializer

logger = logging.getLogger(__name__)


@celery_app.task(queue="default", bind=True)
def send_message(self, distribution_id, message_id, client_phone_number):
    d = services.get_distribution_by_id(distribution_id)
    if d.can_distribute:
        headers = {
            "Authorization": f"Bearer {settings.DISTRIBUTION_PROVIDER_TOKEN}",
            "Content-Type": "application/json"
        }
        data = {
            "id": message_id,
            "text": d.text,
            "phone": client_phone_number
        }

        try:
            result = requests.post(f"{settings.DISTRIBUTION_PROVIDER_URL}{message_id}", json=data, headers=headers)
            if not 200 <= result.status_code < 300:
                raise RequestException
        except RequestException:
            countdown_sec = 300
            logger.info({
                "message_id": message_id,
                "retry_after": countdown_sec
            })
            self.retry(countdown=countdown_sec)
        else:
            m = services.update_message_status(message_id)
            logger.info({
                "message_id": message_id,
                "message status": "sent",
                "client_id": m.client.id
            })


@celery_app.task(queue="default", max_retries=3)
def send_mail_task():
    users = User.objects.filter(is_superuser=True)
    distributions = services.get_all_distributions()
    statistic = StatisticSerializer(services.get_common_statistic(distributions), many=True).data
    string = ""
    for item in statistic:
        string += f"{json.dumps(item['distribution'])}\nstatistic: {json.dumps(item['statistic'])}\n\n"
    send_mail(
        subject="subject",
        recipient_list=[user.email for user in users if user.email],
        from_email=settings.EMAIL_HOST_USER,
        message=string
    )
