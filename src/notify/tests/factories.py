from faker import Faker
from faker_e164.providers import E164Provider
from django.utils.timezone import timedelta, now

import factory.django
from phonenumber_field.phonenumber import PhoneNumber

from notify.models import Distribution, Message, Client


fake = Faker(locale="ru_RU")
fake.add_provider(E164Provider)


def fake_phone_number() -> PhoneNumber:
    phone_str = fake.e164()
    while True:  # Create new phone number that not in database
        if not Client.objects.filter(phone_number=phone_str).exists():
            break
        phone_str = fake.e164()
    return PhoneNumber.from_string(phone_str)


class DistributionFactory(factory.django.DjangoModelFactory):
    selection_data = {"tag__in": ["test"], "mobile_operator_code__in": [919]}
    start_time = now()
    end_time = now() + timedelta(minutes=5)

    class Meta:
        model = Distribution


class ClientFactory(factory.django.DjangoModelFactory):
    tag = "test"
    mobile_operator_code = 919
    phone_number = factory.LazyFunction(fake_phone_number)

    class Meta:
        model = Client


class MessageFactory(factory.django.DjangoModelFactory):
    client = factory.SubFactory(ClientFactory)
    distribution = factory.SubFactory(DistributionFactory)

    class Meta:
        model = Message
