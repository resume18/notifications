import datetime

from django.urls import reverse

from notify.models import Message
from notify.tests.factories import MessageFactory


def test_distribution_info(db, client):
    MessageFactory(status=Message.Status.SENT)
    MessageFactory(status=Message.Status.NOT_SENT)
    response = client.get(reverse("distribution-common-statistic"))
    assert response.status_code == 200
    json = response.json()
    assert json[0]["statistic"][0]["count"] == 1
    assert json[0]["statistic"][1]["count"] == 1
    assert json[1]["statistic"][0]["count"] == 3


def test_distribution_detail_info(db, client):
    m1 = MessageFactory(status=Message.Status.SENT)
    MessageFactory(status=Message.Status.NOT_SENT, distribution=m1.distribution)
    response = client.get(reverse("distribution-detail-statistic", args=(m1.distribution.id, )))
    assert response.status_code == 200
    statistic = response.json()["statistic"]
    assert statistic[0]["count"] == 2
    assert statistic[1]["count"] == 1


def test_invalid_distribution_interval_time(db, client):
    json = {
        "selection_data": {"tag__in": "test", "mobile_operator_code__in": 919},
        "start_time": datetime.datetime.now() + datetime.timedelta(hours=5),
        "end_time": datetime.datetime.now(),
        "text": "dsadasdadasdsada"
    }
    response = client.post("/notify/distribution/", json=json)
    assert response.status_code == 400


def test_invalid_selection_data(db, client):
    json = {
        "selection_data": {"tag__in": "te", "mobile_operator_code__in": 19},
        "start_time": datetime.datetime.now() + datetime.timedelta(hours=5),
        "end_time": datetime.datetime.now(),
        "text": "dsadasdadasdsada"
    }
    response = client.post("/notify/distribution/", json=json)
    assert response.status_code == 400
