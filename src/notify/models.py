from django.utils import timezone
import pytz
from django.contrib.auth.base_user import AbstractBaseUser
from django.core.exceptions import FieldError, ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models import TextChoices, Q, F
from phonenumber_field.modelfields import PhoneNumberField
from phonenumbers import parse
from rest_framework import status


class BaseModel(models.Model):
    created_at = models.DateTimeField("Время создания", auto_now_add=True)
    updated_at = models.DateTimeField("Время обновления", auto_now=True)

    class Meta:
        abstract = True


class Distribution(BaseModel):
    start_time = models.DateTimeField("Время начала рассылки")
    end_time = models.DateTimeField("Время окончания рассылки")
    text = models.CharField(max_length=2000)
    selection_data = models.JSONField("Критерии выбора клиентов", default=dict)

    class Meta:
        verbose_name = "Рассылка"
        verbose_name_plural = "Рассылки"
        constraints = [
            models.CheckConstraint(
                check=Q(start_time__lte=F('end_time')),
                name='start_before_end'
            )
        ]

    def validate_selection_data(self):
        try:
            c = Client.objects.filter(**self.selection_data)
        except (ValueError, FieldError) as e:
            raise ValidationError("Invalid selection data.", code=status.HTTP_400_BAD_REQUEST)
        else:
            if not c:
                raise ValidationError("Matched clients not not found.", code=status.HTTP_404_NOT_FOUND)

    def save(self, *args, **kwargs):
        self.validate_selection_data()
        super().save(*args, **kwargs)

    @property
    def can_distribute(self) -> bool:
        return self.start_time <= timezone.now() <= self.end_time


class Client(BaseModel):
    phone_number = PhoneNumberField(unique=True)
    mobile_operator_code = models.IntegerField(
        validators=[MaxValueValidator(999), MinValueValidator(1)],
        null=True,
        blank=True
    )
    tag = models.CharField(max_length=20, blank=True, null=True)
    timezone = models.CharField(
        max_length=32,
        choices=tuple(zip(pytz.all_timezones, pytz.all_timezones)),
        blank=True,
        null=True
    )

    def set_mobile_operator_code(self):
        parsed = parse(self.phone_number)
        self.mobile_operator_code = parsed.national_number[:3]
        return self.mobile_operator_code

    def save(self, *args, **kwargs):
        if not self.mobile_operator_code:
            self.set_mobile_operator_code()
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = "Клиент"
        verbose_name_plural = "Клиенты"


class Message(models.Model):
    class Status(TextChoices):
        SENT = "sent", "отправлено"
        NOT_SENT = "not sent", "не отправлено"

    status = models.CharField("Статус", max_length=32, choices=Status.choices, default=Status.NOT_SENT)
    created_at = models.DateTimeField("Время создания.", auto_now=True)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    distribution = models.ForeignKey(Distribution, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"
