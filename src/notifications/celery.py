import os
from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'notifications.settings')
celery_app = Celery('notifications')
celery_app.config_from_object("django.conf:settings", namespace='CELERY')
celery_app.conf.beat_schedule = {
    "send_statistic": {
        "task": "notify.tasks.send_mail_task",
        "schedule": crontab(hour=13)
    }
}

celery_app.autodiscover_tasks()
