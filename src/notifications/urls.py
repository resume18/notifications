from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

from notify.admin import admin_site


urlpatterns = [
        path("admin/", admin_site.urls),
        path("notify/", include("notify.urls")),
    ] + static(settings.STATIC_URL)


schema_view = get_schema_view(
    openapi.Info(
        title="Notify",
        default_version="v1",
        description="Distribution.",
        terms_of_service="",
        # contact=openapi.Contact(email=""),
    ),
    public=True,
    permission_classes=(permissions.IsAuthenticated,),
)

urlpatterns += [
    re_path(r"^docs(?P<format>\.json|\.yaml)$", schema_view.without_ui(cache_timeout=0), name="schema-json"),
    re_path(r"^docs/$", schema_view.with_ui("swagger", cache_timeout=0), name="schema-swagger-ui"),
    re_path(r"^redoc/$", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"),
]
